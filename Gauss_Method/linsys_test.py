#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
test for Gauss Method
"""
__version__="0.0.1"
__author__="Kelsey Nudd"
__license__="gpl3"


import sys
import os
import traceback
import argparse
import time
import unittest
import linsys
import numpy as np

# Global variables spare me from putting them in the call of each fcn.
VERBOSE = False
DEBUG = False

class JHException(Exception):
    pass

def warn(s):
    t = 'WARNING: '+s+"\n"
    sys.stderr.write(t)
    sys.stderr.flush()

def error(s):
    t = 'ERROR! '+s
    sys.stderr.write(t)
    sys.stderr.flush()
    sys.exit(10)

import logging
# create file handler which logs even debug messages
log = logging.getLogger()
# Potential logging levels: DEBUG | INFO | WARNING | ERROR | CRITICAL
if DEBUG:
    log.setLevel(logging.DEBUG)  
else:
    log.setLevel(logging.ERROR)  # DEBUG | INFO | WARNING | ERROR | CRITICAL
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - Line: %(lineno)d\n%(message)s')
sh = logging.StreamHandler()
sh.setLevel(logging.ERROR)
sh.setFormatter(formatter)
log.addHandler(sh)
fh = logging.FileHandler(os.path.abspath(os.path.join(
    os.path.dirname(__file__), 
    os.path.basename(__file__).rstrip('.py') + '.log'
)))
fh.setLevel(logging.ERROR)
fh.setFormatter(formatter)
log.addHandler(fh)


# ==============================================
import linsys
class LinearSystemsTestCase(unittest.TestCase):
    """Tests."""

    def test_pivot(self):
        """Test pivot function"""
        matrix = np.array([[1,2],[3,4]])
        x = linsys.pivot(matrix,4,0,1)
        self.assertEqual(np.array_equal(np.array([[1,2],[7,12]]),x),True)

    def test_rescale(self):
        """Test rescale function"""
        matrix = np.array([[1,2],[3,4]])
        x = linsys.rescale(matrix,5,1)
        self.assertEqual(np.array_equal(np.array([[1,2],[15,20]]),x),True)

    def test_swap(self):
        """Test swap function"""
        matrix = np.array([[1,2],[3,4]])
        x = linsys.swap(matrix,0,1)
        self.assertEqual(np.array_equal(np.array([[3,4],[1,2]]),x),True)

        return(True)
        
    

# ===========================================================
def main(args):
    unittest.main()
    

# ===========================================================
if __name__ == '__main__':
    try:
        start_time = time.time()
        # Parser: See http://docs.python.org/dev/library/argparse.html
        parser = argparse.ArgumentParser(description=__doc__+
                                         "\n"+__author__
                                         +" version: "+__version__
                                         +" license: "+__license__)
        parser.add_argument('-D', '--debug', action='store_true', default=DEBUG, help='run debugging code')
        parser.add_argument('-v', '--version', action='version', version=__version__)
        parser.add_argument('-V', '--verbose', action='store_true', default=False, help='verbose output')
        args = parser.parse_args()
        if args.debug:
            fh.setLevel(logging.DEBUG)
            log.setLevel(logging.DEBUG)
        elif args.verbose:
            fh.setLevel(logging.INFO)
            log.setLevel(logging.INFO)
        log.info("%s Started" % parser.prog)
        main(args)
        log.info("%s Ended" % parser.prog)
        log.info("Total running time in seconds: %0.2f" % (time.time() - start_time))
        sys.exit(0)
    except KeyboardInterrupt as e:  # Ctrl-C
        raise e
    except SystemExit as e:  # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)
